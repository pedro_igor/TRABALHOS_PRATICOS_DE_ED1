#include "financas.hpp"
#include "evento.hpp"

struct financa
{
	Gasto **gasto;
	Evento **evento;
	int quant_eventos;
};

Financa* cria_financa( Gasto** gasto)
{
	Financa *f= (Financa*)malloc( sizeof(Financa));

	f->gasto = gasto;
	return f;
}

void set_gastos(Financa* f, Gasto** g)
{
	f->gasto = g;
}

void set_eventos(Financa* f, Evento** e)
{
	f->evento = e;
}

void quantidade_eventos(Financa* financa, int quantidade)
{
	financa->quant_eventos = quantidade;
}

Gasto** get_gastos(Financa* f)
{
	return f->gasto;
}

Evento** get_eventos(Financa* f)
{
	return f->evento;
}

int get_quant_eventos(Financa* financa)
{
	return financa->quant_eventos;
}

void apaga_financas(Financa* f,int quantidade, int quantidade_evento)
{
	//apaga_eventos(get_eventos(f));
	libera_eventos(get_eventos(f), f->quant_eventos);
	apaga_gastos(get_gastos(f),quantidade);
	free(f);
}

void print_financas(Financa* financas)
{
	print_gastos(get_gastos(financas));

	print_evento(get_eventos(financas), financas->quant_eventos);

}