#ifndef DATA_H
#define DATA_H

#include <cstdlib>
typedef struct data Data;

Data* cria_data(int dia, int mes, int ano);
Data* cria_data();

void set_dia( Data* d, int dia );
void set_mes( Data* d, int mes );
void set_ano( Data* d, int ano );

int get_dia( Data* d );
int get_mes( Data* d );
int get_ano( Data* d );

void apaga_data( Data* d );

#endif
