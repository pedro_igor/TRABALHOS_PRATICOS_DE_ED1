#include "gasto.hpp"

struct gasto
{
	string des;
	double valor;
	Data* vencimento;
};

Gasto** criar_gastos()
{
	return new Gasto * [6];
}

Gasto* criar_gasto()
{
	Gasto* aux = new Gasto;
	aux->vencimento = cria_data();
	return aux;
}

Gasto* cria_gasto( string descricao, double valor, Data* vencimento)
{
	Gasto* g = (Gasto*) malloc(sizeof(Gasto));
	
	//if( d == i)
	g->des = descricao;
	
	//g->des = alugel;

	g->vencimento = vencimento;

	g->valor = valor;

	return g;
}

void set_descricao(Gasto* g, string d )
{
	g->des = d;
}

void set_valor(Gasto* g, double valor)
{
	g->valor = valor;
}

void set_vencimento(Gasto* g, Data* data)
{
	g->vencimento = data;
}

double get_valor(Gasto* g)
{
	return g->valor;
}

string get_descricao(Gasto* g)
{
	return string(g->des);
}

Data* get_vencimento(Gasto* g)
{
	return g->vencimento;
}

void ordenar_gastos(Gasto** g, int quantidade)
{
	mergeSort_gastos(g, 0, quantidade - 1);
}

void merge_gastos(Gasto** vetor, int comeco, int meio, int fim)
{
    int com1 = comeco, com2 = meio + 1, comAux = 0, tam = fim - comeco + 1;
	Gasto* vetAux;
	vetAux = (Gasto*)malloc(tam * sizeof(Gasto));
	int dia1,dia2,mes1,mes2;
	while (com1 <= meio && com2 <= fim) {
		dia1 = get_dia(vetor[com1]->vencimento);
		dia2 =  get_dia(vetor[com2]->vencimento);
		mes1 = get_mes(vetor[com1]->vencimento);
		mes2 =  get_mes(vetor[com2]->vencimento);
		if (mes1<mes2){
			vetAux[comAux] = *vetor[com1];
			com1++;
		}
		else if(mes2<mes1)
		{
			vetAux[comAux] = *vetor[com2];
			com2++;
		}
		else if(mes1 == mes2 && dia1>dia2)
		{
			vetAux[comAux] = *vetor[com1];
			com1++;
		}
		else if(mes1 == mes2 && dia1<dia2)
		{
			vetAux[comAux] = *vetor[com2];
			com2++;
		}
		comAux++;
	}

	while (com1 <= meio) {  //Caso ainda haja elementos na primeira metade
		vetAux[comAux] = *vetor[com1];
		comAux++;
		com1++;
	}

	while (com2 <= fim) {   //Caso ainda haja elementos na segunda metade
		vetAux[comAux] = *vetor[com2];
		comAux++;
		com2++;
	}

	for (comAux = comeco; comAux <= fim; comAux++) {    //Move os elementos de volta para o vetor original
		*vetor[comAux] = vetAux[comAux - comeco];
	}

	free(vetAux);
}

void mergeSort_gastos(Gasto** vetor, int comeco, int fim)
{
    if (comeco < fim) {
		int meio = (fim + comeco) / 2;

		mergeSort_gastos(vetor, comeco, meio);
		mergeSort_gastos(vetor, meio + 1, fim);
		merge_gastos(vetor, comeco, meio, fim);
	}
}


void apaga_gasto(Gasto*g)
{
	apaga_data(get_vencimento(g));
	free(g);
}

void apaga_gastos(Gasto**g, int quantidade)
{
	for(int i = 0; i < quantidade; i++)
		apaga_gasto(*g);
	free(g);
}

void print_gastos(Gasto** contas)
{
	for(int i = 0; i < 6; i++)
	{
		cout << "\nTipo da conta: " << get_descricao(contas[i]);
		cout << "\nValor: R$ " << contas[i]->valor;
		cout << "\nVencimento: " << get_dia(contas[i]->vencimento) << " / " 
								<< get_mes(contas[i]->vencimento) << " / "
								<< get_ano(contas[i]->vencimento) << endl;
	}
}