#ifndef FINANCAS_HPP
#define FINANCAS_HPP

#include "gasto.hpp"
#include "evento.hpp"


typedef struct financa Financa;

Financa* cria_financa( Gasto** g);

void set_gastos(Financa* f, Gasto** g);

void set_eventos(Financa* f, Evento** e);

void quantidade_eventos(Financa* financa, int quantidade);

Gasto** get_gastos(Financa* f);

Evento** get_eventos(Financa* f);

int get_quant_eventos(Financa* financa);

void apaga_financas(Financa* f, int quantidade, int quantidade_evento);

void print_financas(Financa* financas);

#endif