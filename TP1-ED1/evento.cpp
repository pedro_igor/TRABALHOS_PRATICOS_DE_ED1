//implementacao do evento.hpp
#include "evento.hpp"
#include <string>

using std::string;

struct evento
{
    string nome_evento;
    double dinheiro;
    double gastos;
    double lucro;
};

Evento** cria_eventos(int quantidade_Eventos)
{
    return new Evento * [quantidade_Eventos];  
}

Evento* cria_evento(string nome_evento, double dinheiro, double gastos, double lucro)
{
    Evento* aux = new Evento;

    if(aux == NULL)
    {
        printf("ERRO: Memoria insuficiente\n");
        exit(1);
    }

    aux->nome_evento = nome_evento;
    aux->dinheiro = dinheiro;
    aux->gastos = gastos;
    aux->lucro = lucro;

    return aux;
}

void libera_eventos(Evento** eventos, int quantidade_evento)
{
    for(int i = 0; i < quantidade_evento; i++)
    {
        delete eventos[i];
    }

    delete eventos;
}

void set_nome_evento(Evento* evento, string nome_evento)
{
    evento->nome_evento= nome_evento;
}

void set_dinheiro(Evento* evento, double dinehiro)
{
    evento->dinheiro = dinehiro;
}

void set_gastos(Evento* evento, double gasto)
{
    evento->gastos = gasto;
}

void set_lucro(Evento* evento, double lucro)
{
    evento->lucro = lucro;
}

string get_nome_evento(Evento* evento)
{
    return string(evento->nome_evento);
}

double get_dinheiro(Evento* evento)
{
    return evento->dinheiro;
}

double get_gastos(Evento* evento)
{
    return evento->gastos;
}

double get_lucro(Evento* evento)
{
    return evento->lucro;
}

int busca_evento(Evento** eventos, int quantidade_evento, string nome_evento)
{
    return PesquisaBinaria_evento(nome_evento, eventos, 0, quantidade_evento);
}

void ordernar_evento(Evento** eventos, int quant_evento)
{
    mergeSort_evento(eventos, 0, quant_evento - 1);
}

void merge_evento(Evento** vetor, int comeco, int meio, int fim)
{
int com1 = comeco, com2 = meio + 1, comAux = 0, tam = fim - comeco + 1;
	Evento* vetAux;
	vetAux = (Evento*)malloc(tam * sizeof(Evento));

	while (com1 <= meio && com2 <= fim) {
		if (vetor[com1]->nome_evento < vetor[com2]->nome_evento) {
			vetAux[comAux] = *vetor[com1];
			com1++;
		}
		else {
			vetAux[comAux] = *vetor[com2];
			com2++;
		}
		comAux++;
	}

	while (com1 <= meio) {  //Caso ainda haja elementos na primeira metade
		vetAux[comAux] = *vetor[com1];
		comAux++;
		com1++;
	}

	while (com2 <= fim) {   //Caso ainda haja elementos na segunda metade
		vetAux[comAux] = *vetor[com2];
		comAux++;
		com2++;
	}

	for (comAux = comeco; comAux <= fim; comAux++) {    //Move os elementos de volta para o vetor original
		*vetor[comAux] = vetAux[comAux - comeco];
	}

	free(vetAux);
}

void mergeSort_evento(Evento** vetor, int comeco, int fim)
{
     if (comeco < fim) {
		int meio = (fim + comeco) / 2;

		mergeSort_evento(vetor, comeco, meio);
		mergeSort_evento(vetor, meio + 1, fim);
		merge_evento(vetor, comeco, meio, fim);
	}
}

int PesquisaBinaria_evento(string nome_evento, Evento** v, int e, int d)
{
    	int meio = (e + d)/2;
	if (v[meio]->nome_evento == nome_evento)
		return meio;
	if (e >= d)
		return -1; // não encontrado
	else
		if (v[meio]->nome_evento < nome_evento)
			return PesquisaBinaria_evento(nome_evento, v, meio+1, d);
		else
			return PesquisaBinaria_evento(nome_evento, v, e, meio-1);
}

void print_evento(Evento** evento, int quant)
{
	if(evento == NULL)
	{
		cout << "\nERRO: Evento nao encontado" << endl;
	}
	else
	{
		for (int i = 0; i < quant; i++)
		{

			cout << "\nNome: " << evento[i]->nome_evento
				<< "\nValor Total: R$" << evento[i]->dinheiro
				<< "\nDispesas: R$" << evento[i]->gastos
				<< "\nLucro: R$" << evento[i]->lucro
				<< endl;

		}
	}
}