#include "recibo.hpp"

struct recibo
{
	Data* prazo;
	Data* data;
	int status;
};

Recibo* cria_recibo( Data* prazo, Data* data, int status )
{
	Recibo *r =(Recibo*)malloc( sizeof(Recibo));

	r->prazo = prazo;
	r->data = data;
	r->status = status;

	return r;
}

void set_prazo(Recibo* r, Data* data)
{
	r->data = data;
}

void set_data(Recibo* r, Data* data)
{
	r->data = data;
}

void set_status(Recibo* r, int status )
{
	r->status = status;
}

Data* get_data(Recibo* r)
{
	return r->data;
}

Data* get_prazo(Recibo* r)
{
	return r->prazo;
}

int get_status(Recibo* r)
{
	return r->status;
}

void apaga_recibo(Recibo* r)
{
	apaga_data(get_data(r));
	apaga_data(get_prazo(r));
	free(r);
}