#include "data.hpp"

struct data{
	int dia;
	int mes;
	int ano;
};

Data* cria_data(int dia, int mes, int ano)
{
	Data* data = (Data*)malloc(sizeof(Data));

	data->dia = dia;
	data->mes = mes;
	data->ano = ano;

	return data;
}

Data* cria_data()
{
	return new Data;
}
void set_dia( Data* d, int dia )
{
	d->dia = dia;
}

void set_mes( Data* d, int mes )
{
	d->mes = mes;
}

void set_ano( Data* d, int ano )
{
	d->ano = ano;
}

int get_dia( Data* d )
{
	return d->dia;
}

int get_mes( Data* d )
{
	return d->mes;
}

int get_ano( Data* d )
{
	return d->ano;
}

void apaga_data( Data* d )
{
	free(d);
}

