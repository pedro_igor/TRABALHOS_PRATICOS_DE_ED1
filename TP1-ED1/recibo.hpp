#ifndef RECIBO_HPP
#define RECIBO_HPP

#include <string>
#include "data.hpp"

using namespace std;
using std::string;

typedef struct recibo Recibo;

Recibo* cria_recibo( Data* prazo, Data* data, int status );

void set_prazo(Recibo* r, Data* data);

void set_data(Recibo* r, Data* data);

void set_status(Recibo* r, int status );

Data* get_data(Recibo* r);

Data* get_prazo(Recibo* r);

int get_status(Recibo* r);

void apaga_recibo(Recibo* r);

#endif

