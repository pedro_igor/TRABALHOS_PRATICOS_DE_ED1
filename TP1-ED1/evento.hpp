#ifndef EVENTO_HPP
#define EVENTO_HPP

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>

using namespace std;
using std::string;

typedef struct evento Evento;

Evento** cria_eventos(int quantidade_Eventos);

Evento* cria_evento(string nome_evento, double dinheiro, double gastos, double lucro);

void libera_eventos(Evento** eventos, int quantidade_evento);

void set_nome_evento(Evento* evento, string nome_evento);

void set_dinheiro(Evento* evento, double dinehiro);

void set_gastos(Evento* evento, double gasto);

void set_lucro(Evento* evento, double lucro);

string get_nome_evento(Evento* evento);

double get_dinheiro(Evento* evento);

double get_gastos(Evento* evento);

double get_lucro(Evento* evento);

int busca_evento(Evento** eventos, int quantidade_evento, string nome_evento);

void ordernar_evento(Evento** eventos, int quant_evento);

void merge_evento(Evento** vetor, int comeco, int meio, int fim);

void mergeSort_evento(Evento** vetor, int comeco, int fim);

int PesquisaBinaria_evento(string nome_evento, Evento** v, int e, int d);//d = direita, e = esqueda

void print_evento(Evento** evento, int quant);

#endif // !EVENTO_HPP
