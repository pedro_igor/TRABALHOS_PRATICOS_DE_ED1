#ifndef GASTO_HPP
#define GASTO_HPP

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "data.hpp"
#include <string>

using namespace std;
using std::string;

typedef struct gasto Gasto;

//enum descricao {alugel, internet, telefone, agua, alimentos, luz}; # nao consigimos manipular ENUM
//typedef enum descricao Descricao;

Gasto** criar_gastos();

Gasto* criar_gasto();

Gasto* cria_gasto( string d, double valor, Data* vencimento);

void set_descricao(Gasto* g, string d);

void set_valor(Gasto* g, double valor);

void set_vencimento(Gasto* g, Data* data);

double get_valor(Gasto* g);

string get_descricao(Gasto* g);

Data* get_vencimento(Gasto* g);

void ordenar_gastos(Gasto** g, int quantidade);

void merge_gastos(Gasto** vetor, int comeco, int meio, int fim);

void mergeSort_gastos(Gasto** vetor, int comeco, int fim);

void apaga_gasto(Gasto* g);

void apaga_gastos(Gasto**g, int quantidade);

void print_gastos(Gasto** contas);
#endif 






