//Implemetação do republica.hpp
#include "republica.hpp"

struct republica
{
    string nome_republica;
    int capacidade;
    int quantidade_atual;
    string bairro;
    string tipo_genero;
    string tipo;
	Morador** moradores;
    Gasto** contas;
	Evento** eventos;
	Financa* financa;
	int quantidade_eventos;
};

Republica** cria_Republicas(int quant_republica)
{
    return new Republica*[quant_republica];
}

Republica* cria_Republica(string nome_rep, int capacidade, int quantidade_atual, string bairro, string tipo_genero, string tipo)
{
	Republica* aux = new Republica;
	
	aux->moradores = NULL;
	aux->contas = NULL;
	aux->eventos = NULL;
	aux->financa = NULL;

	if (aux == NULL)
	{
		cout << "ERRO: Memoria insuficiente." << endl;
		exit(1);
	}

	Morador** moradores = cria_moradores(capacidade);//variavel auxiliar para moradores

	for(int i = 0; i < capacidade; i++)
	{
		moradores[i] = cria_morador(/*NULL, NULL, NULL, NULL, NULL, NULL, NULL*/);
	}
	
	aux->nome_republica = nome_rep;
	aux->capacidade = capacidade;
	aux->quantidade_atual = quantidade_atual;
	aux->bairro = bairro;
	aux->tipo_genero = tipo_genero;
	aux->tipo = tipo;
	aux->moradores = moradores;
	aux->quantidade_eventos = 0;

	return aux;
}

void libera_republicas(Republica** republica, int quantidade_republicas)
{
    for(int i = 0; i < quantidade_republicas; i++)
    {
        delete republica[i];
    }

    delete republica;
}

void set_nome_republica(Republica* republica, string nome_republica)
{
    republica->nome_republica = nome_republica;
}

void set_capacidade(Republica* republica, int capacidade)
{
    republica->capacidade = capacidade;
}

void set_quantide(Republica* republica, int quantidade)
{
    republica->quantidade_atual= quantidade;
}

void set_bairro(Republica* republica, string bairro)
{
    republica->bairro = bairro;
}

void set_tipo_genero(Republica* republica, string tipo_genero)
{
    republica->tipo_genero = tipo_genero;
}

void set_moradores(Republica* republica, Morador** moradores)
{
	republica->moradores = moradores;
}

void set_morador(Republica* republica, Morador* morador, int indice)
{
	republica->moradores[indice] = morador;
}

void set_tipo(Republica* republica, string tipo)
{
    republica->tipo = tipo;
}

//void set_gastos(Republica* republica, Gasto** contas)
//{
//	republica->contas = contas;
//}
//
//void set_eventos(Republica* republica, Evento** eventos)
//{
//	republica->eventos = eventos;
//}
//
//void set_quant_evento(Republica* republica, int quantidade)
//{
//	republica->quantidade_eventos = quantidade;
//}

void set_financas(Republica* republica, Financa* financa)
{
	republica->financa = financa;
}


string get_nome_republica(Republica* republica)
{
    return string(republica->nome_republica);
}

int get_capacidade(Republica* republica)
{
    return republica->capacidade;
}

int get_quantidade_atual(Republica* republica)
{
    return republica->quantidade_atual;
}

string get_bairro(Republica* republica)
{
    return string(republica->bairro);
}

string get_tipo_genero(Republica* republica)
{
    return string(republica->tipo_genero);
}

string get_tipo(Republica* republica)
{
    return string(republica->tipo);
}

Morador** get_moradores(Republica* republica)
{
	return republica->moradores;
}

Morador* get_morador(Republica* republica, int indice)
{
	return republica->moradores[indice];
}

//Gasto** get_gastos(Republica* republica)
//{
//	return republica->contas;
//}
//
//Evento** get_evento(Republica* republica)
//{
//	return republica->eventos;
//}

Financa* get_financa(Republica* republica)
{
	return republica->financa;
}

//int get_quant_evento(Republica* republica)
//{
//	return republica->quantidade_eventos;
//}

void ordenar_republica(Republica** republicas, int quant_republica)
{
	//cout << "ERRO 1" << endl;
    mergeSort_republica(republicas, 0, quant_republica - 1);
		//cout << "ERRO 4" << endl;
}

void merge_republica(Republica** vetor, int comeco, int meio, int fim)
{
		//cout << "ERRO 2" << endl;
    int com1 = comeco, com2 = meio + 1, comAux = 0, tam = fim - comeco + 1;
	Republica* vetAux;
	vetAux = (Republica*)malloc(tam * sizeof(Republica));
		//cout << "ERRO 3" << endl;

	while (com1 <= meio && com2 <= fim) {
		if (vetor[com1]->nome_republica < vetor[com2]->nome_republica) {
			vetAux[comAux] = *vetor[com1];
			com1++;
		}
		else {
			vetAux[comAux] = *vetor[com2];
			com2++;
		}
		comAux++;
	}

	while (com1 <= meio) {  //Caso ainda haja elementos na primeira metade
		vetAux[comAux] = *vetor[com1];
		comAux++;
		com1++;
	}

	while (com2 <= fim) {   //Caso ainda haja elementos na segunda metade
		vetAux[comAux] = *vetor[com2];
		comAux++;
		com2++;
	}

	for (comAux = comeco; comAux <= fim; comAux++) {    //Move os elementos de volta para o vetor original
		*vetor[comAux] = vetAux[comAux - comeco];
	}

	free(vetAux);
}

void mergeSort_republica(Republica** vetor, int comeco, int fim)
{
    if (comeco < fim) {
		int meio = (fim + comeco) / 2;

		mergeSort_republica(vetor, comeco, meio);
		mergeSort_republica(vetor, meio + 1, fim);
		merge_republica(vetor, comeco, meio, fim);
	}
}

int busca_republica(Republica** republica, int quant_republica, string nome_republica)
{
    return PesquisaBinaria_republica(nome_republica,republica, 0, quant_republica);
}

int PesquisaBinaria_republica (string  nome, Republica** v, int e, int d)
{
	int meio = (e + d)/2;
	if (v[meio]->nome_republica == nome)
		return meio;
	if (e >= d)
		return -1; // não encontrado
	else
		if (v[meio]->nome_republica < nome)
			return PesquisaBinaria_republica(nome, v, meio+1, d);
		else
			return PesquisaBinaria_republica(nome, v, e, meio - 1); 
}

void print_republica(Republica* republica)
{
	//system("clear");
	if(republica == NULL)
	{
		cout << "ERRO: Republica nao encontrada" << endl;
	}
	else
	{
		cout << "\nNome: " << republica->nome_republica
			<< "\nCapacidade maxima de moradores: " << republica->capacidade
			<< "\nQuantidade Atual: " << republica->quantidade_atual
			<< "\nBairro: " << republica->bairro
			<< "\nFeminina ou Masculina ou Mista: " << republica->tipo_genero
			<< "\nFederal ou Particular: " << republica->tipo
			<< endl;
		
		print_financas(get_financa(republica));
	}
	
}